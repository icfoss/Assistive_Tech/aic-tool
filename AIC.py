import AIC_UI as Ui
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QMouseEvent
from AIC_UI import Ui_MainWindow
import autocomplete
import pyttsx3
import threading
from espeak import espeak

autocomplete.load()
displayString = ""
_speak_letters = ''
_auto_speak = ''
_deleteCharacter = False
_timerSecondCount = 0


def buttonTrigger(self):
    self.pushButton_2.clicked.connect(lambda: self.setText('q'))
    self.pushButton_3.clicked.connect(lambda: self.setText('w'))
    self.pushButton_4.clicked.connect(lambda: self.setText('e'))
    self.pushButton_5.clicked.connect(lambda: self.setText('r'))
    self.pushButton_6.clicked.connect(lambda: self.setText('t'))
    self.pushButton_7.clicked.connect(lambda: self.setText('y'))
    self.pushButton_8.clicked.connect(lambda: self.setText('u'))
    self.pushButton_9.clicked.connect(lambda: self.setText('i'))
    self.pushButton_10.clicked.connect(lambda: self.setText('o'))
    self.pushButton_11.clicked.connect(lambda: self.setText('p'))

    self.pushButton_12.clicked.connect(lambda: self.setText('a'))
    self.pushButton_13.clicked.connect(lambda: self.setText('s'))
    self.pushButton_14.clicked.connect(lambda: self.setText('d'))
    self.pushButton_15.clicked.connect(lambda: self.setText('f'))
    self.pushButton_16.clicked.connect(lambda: self.setText('g'))
    self.pushButton_17.clicked.connect(lambda: self.setText('h'))
    self.pushButton_18.clicked.connect(lambda: self.setText('j'))
    self.pushButton_19.clicked.connect(lambda: self.setText('k'))
    self.pushButton_20.clicked.connect(lambda: self.setText('l'))

    self.pushButton_21.clicked.connect(lambda: self.setText('z'))
    self.pushButton_22.clicked.connect(lambda: self.setText('x'))
    self.pushButton_23.clicked.connect(lambda: self.setText('c'))
    self.pushButton_24.clicked.connect(lambda: self.setText('v'))
    self.pushButton_25.clicked.connect(lambda: self.setText('b'))
    self.pushButton_26.clicked.connect(lambda: self.setText('n'))
    self.pushButton_27.clicked.connect(lambda: self.setText('m'))
    self.backspace.clicked.connect(lambda: self.setText('\b'))

    self.pushButton_28.clicked.connect(lambda: self.sayText() if not _auto_speak  else self.plainTextEdit.clear())
    self.pushButton_29.clicked.connect(lambda: self.setText(' '))
    self.SpeakLetters.clicked.connect(lambda: self.check_checkBoxes())
    self.AutoSpeak.clicked.connect(lambda: self.check_checkBoxes())
    self.plainTextEdit.textChanged.connect(lambda: self.updateText())


def setText(self, alaphabet):
    global _speak_letters
    global _deleteCharacter
    if alaphabet == '\b':
        _deleteCharacter = True
        self.plainTextEdit.clear()
        return 0
    else:
        self.plainTextEdit.setFocus()
        self.plainTextEdit.insertPlainText(alaphabet)

    if _speak_letters:
        espeak.synth(str(alaphabet))
        #Speechengine.say(str(alaphabet))
        #Speechengine.runAndWait()


def updateText(self):
    global displayString
    global _auto_speak
    global _deleteCharacter
    if not _deleteCharacter:
        displayString = self.plainTextEdit.toPlainText()
    else:
        if displayString:
            displayString = displayString[0:-1]
        _deleteCharacter = False
        self.setText(displayString)
    #self.prediction()
    if displayString[-1:] == "\n":
        displayString = displayString[0:-1]
        displayString = displayString+" "
    if _auto_speak and displayString[-1:] == " ":
        self.sayText()
        timer = threading.Timer(5,self.plainTextEdit.clear)
        timer.start()

def prediction(self):
    global displayString
    lastword = ''
    presentword = displayString.split(' ')[-1]
    print("last word: ", presentword)
    if len(displayString.split(' ')) > 1:
        lastword = displayString.split(' ')[-2]
        print("2ndlast word: ", lastword)
    wordlist = autocomplete.predict(lastword, presentword)
    print(wordlist)

def check_checkBoxes(self):
    global _speak_letters
    global _auto_speak

    if self.SpeakLetters.isChecked():
        _speak_letters = True
        print("Speak Letter: ", _speak_letters)
    elif not self.SpeakLetters.isChecked():
        _speak_letters = False
        print("Speak Letter: ", _speak_letters)

    if self.AutoSpeak.isChecked():
        self.pushButton_28.hide
        _auto_speak = True
        print("Auto Speak: ", _auto_speak)
        self.pushButton_28.setText("Erase")
    elif not self.AutoSpeak.isChecked():
        self.pushButton_28.show
        _auto_speak = False
        self.pushButton_28.setText("Speak")
        print("Auto Speak: ", _auto_speak)


def sayText(self):
    global displayString
    if len(displayString) == 0:
        timer.end()
        return 0
    # espeak.synth(displayString)
    #
    # # tts = gTTS(displayString)
    # # tts.save('temp.mp3')
    # # playsound.playsound('temp.mp3', True)

    if not _auto_speak:
        if len(displayString)>0:
            espeak.synth(str(displayString))
        displayString = ""
        self.plainTextEdit.clear()
    if _auto_speak:
        print("String:", displayString.split(' ')[-2])
        if len(displayString)>0:
            espeak.synth(str(displayString.split(' ')[-2]))


def updateTimer(self):
    global _timerSecondCount
    print("updating timer: ", _timerSecondCount)
    _timerSecondCount = _timerSecondCount + 1
    if _timerSecondCount >=  5:
        self.plainTextEdit.clear()
        _timerSecondCount = 0


Ui_MainWindow.sayText = sayText
Ui_MainWindow.setText = setText
Ui_MainWindow.buttonTrigger = buttonTrigger
Ui_MainWindow.check_checkBoxes = check_checkBoxes
Ui_MainWindow.updateText = updateText
Ui_MainWindow.prediction = prediction
Ui_MainWindow.updateTimer = updateTimer


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    ui.buttonTrigger()
    MainWindow.show()
    sys.exit(app.exec_())
