# AIC Tool

[Test Project] [Strictly Under Development] AIC tool that can be used for communication.

Command to convert ```.ui``` to ```.py``` :

```
pyuic5 AIC.ui -o AIC_UI.py -x
```

How to run the program

```
python3 AIC.py
```
